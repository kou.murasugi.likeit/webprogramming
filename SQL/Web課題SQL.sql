﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE user(
id SERIAL,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) UNIQUE NOT NULL,
birth_date date NOT NULL,
password varchar(255) NOT NULL,
create_date datetime NOT NULL,
update_date datetime NOT NULL
);

INSERT INTO user (id,login_id,name,birth_date,
password,create_date,update_date)VALUES(1,'admin','管理者','1996-11-10',1,NOW(),NOW());

INSERT INTO user (id,login_id,name,birth_date,
password,create_date,update_date)VALUES(2,'admin2','田中太郎','1996-11-10',2,NOW(),NOW());

INSERT INTO user (id,login_id,name,birth_date,
password,create_date,update_date)VALUES(3,'admin3','佐藤二朗','1996-11-10',3,NOW(),NOW());

INSERT INTO user (id,login_id,name,birth_date,
password,create_date,update_date)VALUES(4,'admin4','佐川真司','1996-11-10',4,NOW(),NOW());

DROP DATABASE usermanagement;

SELECT*FROM user WHERE id=1;

UPDATE user SET password=?,name=?,birth_date=? WHERE id=?;

DELETE FROM user WHERE id=?;

SELECT * FROM user WHERE login_id = ?;

SELECT * FROM user WHERE login_id=request.getParameter("loginId");

SELECT * FROM user WHERE 
