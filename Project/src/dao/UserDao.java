package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String result = a(password);
			System.out.println(result);

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user Where NOT id=1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 情報を登録する処理
	 */
	public void insert(String loginId, String password, String userName, String birth) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			String result = a(password);

			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,NOW(),NOW())";
			// ステートメント生成                  　　　　　　//↑カラムの項目の名前
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			stmt.setString(2, result);
			stmt.setString(3, userName);
			stmt.setString(4, birth);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * IDに紐づくユーザ情報を返す
	 */ //↓都度ここを変更する
	public User findByDetail(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";
			//↑ここも変更する（先にSQLで正常に動作するか確認する)

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);//idはString idのid
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String passwordDate = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(loginIdDate, nameDate, birthDate, passwordDate, createDate, updateDate);
			//上記のコンストラクタ作成した　

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * IDに紐づくユーザ情報を返す
	 */ //↓都度ここを変更する
	public User findByUpdate(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";
			//↑ここも変更する（先にSQLで正常に動作するか確認する)

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);//idはString idのid
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idDate = rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			return new User(idDate, loginIdDate, nameDate, birthDate);
			//上記のコンストラクタ作成した　

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー更新
	public void UPDATE(String password, String userName, String birth, String loginId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			String result = a(password);

			// 実行SQL文字列定義
			String insertSQL = "UPDATE user SET password=?,name=?,birth_date=? WHERE login_id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, result);
			stmt.setString(2, userName);
			stmt.setString(3, birth);
			stmt.setString(4, loginId);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	//ユーザー更新　パスワード未入力時
	public void UPDATE2(String userName, String birth, String loginId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "UPDATE user SET name=?,birth_date=? WHERE login_id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, userName);
			stmt.setString(2, birth);
			stmt.setString(3, loginId);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	//↓都度ここを変更する
	public User findByDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id=?;";
			//↑ここも変更する（先にSQLで正常に動作するか確認する)

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);//idはString idのid
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			return new User(loginIdDate, nameDate, birthDate);
			//上記のコンストラクタ作成した

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー削除
	public void DELETE(String loginId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM user WHERE login_id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの削除に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	//新規登録　ログインID　
	public User findByRegisterInfo(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findSearch(String login_IdP,String nameP,String startDate,String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user Where NOT id=1";

			if (!login_IdP.equals("")) {
				sql += " AND login_id = '" + login_IdP + "'";
			}

			if (!nameP.equals("")) {
				sql += " AND name LIKE '%" + nameP + "%'";

			}

			if (!startDate.equals("")) {
				sql += " AND  birth_date >= '" + startDate + "'";

			}

			if (!endDate.equals("")) {
				sql += " AND birth_date < '" + endDate + "'";

			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public static String a(String password) throws NoSuchAlgorithmException {
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		return result;
	}
}
