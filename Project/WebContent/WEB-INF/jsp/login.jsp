<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<p></p>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<div class="row">
		<div class="offset-md-1 offset">
			<div class="container">
				<h1>ログイン画面</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-1"></div>
			<form action="LoginServlet" method="post">
				<div class="form-group">
					<label for="exampleInputEmail1">ログインID</label> <input type=""
						name="loginId" class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" placeholder="Login ID"> <small
						id="emailHelp" class="form-text text-muted"></small>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">パスワード</label> <input
						type="password" name="password" class="form-control"
						id="exampleInputPassword1" placeholder="Password">
				</div>
				<div class="form-group form-check">
					<input type="checkbox" class="form-check-input" id="exampleCheck1">
					<label class="form-check-label" for="exampleCheck1">確認</label>
				</div>
				<button type="submit" class="btn btn-primary">ログイン</button>
			</form>
		</div>
</body>

</html>