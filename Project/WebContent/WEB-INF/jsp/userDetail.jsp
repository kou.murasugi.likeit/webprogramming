<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ユーザー詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
	<p></p>
	<p></p>
	<div class="row">
		<div class="offset-md-1 offset">
			<h5>${userInfo.name}
				<a class="btn btn-dark" href="LogoutServlet">ログアウト</a>
			</h5>
			<h1>ユーザー情報詳細参照</h1>
			<p></p>
			<h5>ログインID ${user.loginId}</h5>
			<p></p>
			<h5>ユーザー名 ${user.name}</h5>
			<p></p>
			<h5>生年月日 ${user.birthDate}</h5>
			<p></p>
			<h5>登録日時 ${user.createDate}</h5>
			<p></p>
			<h5>更新日時 ${user.updateDate}</h5>
			<p></p>
			<form action="UserListServlet" method="get">
				<button type="submit" class="btn btn-link">戻る</button>
			</form>
</body>
</html>