<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ユーザー削除画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
	<div class="row">
		<div class="offset-md-1 offset">
			<h5>
				${userInfo.name} <a class="btn btn-dark" href="LogoutServlet">ログアウト</a>
			</h5>
			<h1>ユーザー削除確認</h1>
			<p></p>
			<form action="UserDeleteServlet" method="post">
				<h5>ログインID ${user.loginId}</h5>
				<input type="hidden" value="${user.loginId}" name="login_id">
				<p></p>
				<h5>を本当に削除してよろしいでしょうか。</h5>
				<p></p>
				<button type="submit" class="btn btn-dark">キャンセル</button>
				<button type="submit" class="btn btn-dark">OK</button>
			</form>
</body>
</html>