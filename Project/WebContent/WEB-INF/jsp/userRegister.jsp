<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
	<div class="row">
		<div class="offset-md-1 offset">
			<h5>${userInfo.name}<a class="btn btn-dark" href="LogoutServlet">ログアウト</a>
			</h5>
			<p></p>
			<h1>ユーザー新規登録</h1>
			<p></p>
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
			<form action="RegisterServlet" method="post">
				<div class="form-group">
					<label for="exampleInputEmail1">ログインID </label> <input type=""
						name="loginId" class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" value=${loginId}> <small
						id="emailHelp" class="form-text text-muted"></small>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">パスワード </label> <input
						type="password" name="password" class="form-control"
						id="exampleInputPassword1" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">パスワード（確認）</label> <input
						type="password" name="password2" class="form-control"
						id="exampleInputPassword1" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">ユーザー名 </label> <input
						type="text" name="userName" class="form-control"
						id="exampleInputPassword1" value=${userName}>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">生年月日 </label> <input type="date"
						name="birth_date" class="form-control" id="exampleInputPassword1"
						value=${birth}>
				</div>
				<p></p>
				<input type="submit" class="btn btn-dark" value="登録">
				<p></p>
			</form>
			<form action="UserListServlet" method="get">
				<button type="submit" class="btn btn-link">戻る</button>
			</form>
</body>
</html>