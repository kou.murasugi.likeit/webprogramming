<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ユーザー更新画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
	<div class="row">
		<div class="offset-md-1 offset">
			<div class="container">

				<h5>
					${userInfo.name} <a class="btn btn-dark" href="LogoutServlet">ログアウト</a>
				</h5>
				<h1>ユーザー情報更新</h1>
				<p></p>
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<form action="UserUpdateServlet" method="post">
					<input type="hidden" value="${user.id}" name="id">
					<h5>ログインID ${user.loginId}</h5>
					<input type="hidden" value="${user.loginId}" name="login_id">
					<p></p>

					<div class="form-group">
						<label for="exampleInputPassword1">パスワード </label> <input
							type="password" class="form-control" id="exampleInputPassword1"
							placeholder="Password" name="password">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">パスワード（確認）</label> <input
							type="password" class="form-control" id="exampleInputPassword1"
							placeholder="Password" name="password2">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">ユーザー名 </label> <input
							type="text" class="form-control" id="exampleInputPassword1"
							value=${user.name } name="userName">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">生年月日 </label> <input
							type="text" class="form-control" id="exampleInputPassword1"
							value=${user.birthDate } name="birth_date">
					</div>
					<p></p>
					<button type="submit" class="btn btn-dark">更新</button>
				</form>
				<p></p>
				<form action="UserListServlet" method="get">
					<button type="submit" class="btn btn-link">戻る</button>
				</form>
</body>
</html>