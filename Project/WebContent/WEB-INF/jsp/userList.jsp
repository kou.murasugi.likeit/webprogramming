<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>

<meta charset="UTF-8">

<title>ユーザー一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">



</head>
<body>
	<div class="row">
		<div class="offset-md-1 offset">
			<h5>
				<td>${userInfo.name}</td> <a class="btn btn-dark"
					href="LogoutServlet">ログアウト</a>
			</h5>
			<h1>ユーザー一覧</h1>
			<p></p>
			<div class="row">
				<div class="col-sm-10">
					<a class="btn btn-dark" href="RegisterServlet">新規登録</a>
					<p></p>
				</div>
			</div>
			<form action="UserListServlet" method="post">
				<div class="form-group">
					<label for="exampleInputEmail1">ログインID</label> <input type=""
						class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" placeholder="Login ID"name="login_IdP"> <small
						id="emailHelp" class="form-text text-muted"></small>
				</div>

				<div class="form-group">
					<label for="exampleInputEmail1">ユーザー名</label> <input type=""
						class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" placeholder="User ID"name="nameP"> <small
						id="emailHelp" class="form-text text-muted"></small>
				</div>

				<p></p>

				<div>
					<label for="exampleInputEmail1">生年月日</label>
					<input type="date"name="startDate">

					 ～ <label for="exampleInputEmail1">生年月日</label>
					 <input type="date"name="endDate">
					<br> <br>
				</div>
				<button type="submit" class="btn btn-primary">検索</button>
			</form>
			<p></p>
			<table class="table table-sm table-dark">
				<thead>
					<tr>
						<th scope="col">ログインID</th>
						<th scope="col">ユーザー名</th>
						<th scope="col">生年月日</th>
						<th scope="col"></th>
					</tr>
				</thead>

				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>ログインID</th>
								<th>ユーザ名</th>
								<th>生年月日</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${userList}">
								<tr>
									<td>${user.loginId}</td>
									<td>${user.name}</td>
									<td>${user.birthDate}</td>
									<!-- TODO 未実装；ログインボタンの表示制御を行う -->
									<td><c:if test="${userInfo.loginId=='admin'}">
											<a class="btn btn-primary"
												href="UserDetailServlet?id=${user.id}">詳細</a>
											<a class="btn btn-success"
												href="UserUpdateServlet?id=${user.id}">更新</a>
											<a class="btn btn-danger"
												href="UserDeleteServlet?id=${user.id}">削除</a>
										</c:if> <c:if test="${userInfo.loginId!='admin'}">
											<a class="btn btn-primary"
												href="UserDetailServlet?id=${user.id}">詳細</a>
										</c:if> <c:if test="${userInfo.loginId==user.loginId}">
											<a class="btn btn-success"
												href="UserUpdateServlet?id=${user.id}">更新</a>
										</c:if></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				</div>
				</div>
				</div>
</body>
</html>

